﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;
using System.Net.Sockets;

using Jayrock;
using Jayrock.Json;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using System.Globalization;

using System.Text;
using System.Threading;

// http://developer.neurosky.com/docs/doku.php?id=thinkgear_connector_development_guide
// State object for reading client data asynchronously

namespace NeuroSkyLoad
{
    public class BCI
    {
        float speed, frequency; 

        List<float> meds;
        List<float> atts;

        public BCI()
        {
            meds = new List<float>();
            meds.Add(50);
            atts = new List<float>();
            atts.Add(50);
            speed = 10;
            frequency = 6;
        }

        public void AddData(float att, float med)
        {
            if ((meds != null ) && (atts != null))
            {
                int max = 3;
                if (meds.Count >= max)
                {
                    meds.RemoveAt(0);
                }

                if (atts.Count >= max)
                {
                    atts.RemoveAt(0);
                }

                meds.Add(med);
                atts.Add(att);
            }
        }

        public float getSpeed()
        {
            float avg = 0;
            if ((meds != null) && (atts != null))
            {
                avg = atts.Average();
            }

            speed = 15 - (avg / 10);

            return speed;
        }

        public float getFrequency()
        {
            float avg = 0;
            if ((meds != null) && (atts != null))
            {
                avg = meds.Average();
            }

            frequency = (int) 12 - (avg * avg / 1000);
            return frequency;
        }


    }

    public class StateObject
    {
        public Socket workSocket = null;
        public const int BufferSize = 2048;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
    }

    public class NeuroDataEventArgs : EventArgs
    {
        public string d { get; set; }
    }

    public class NeuroskyGet
    {
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        static IPHostEntry ipHostInfoL;
        static IPAddress ipAddressL;
        static IPEndPoint localEndPointL;

        static TcpClient client;
        static Stream stream;
        //static AsynchronousSocketListener listener;

        // Building command to enable JSON output from ThinkGear Connector (TGC)
        //EventHandler<NeuroDataEventArgs> handler;

        public delegate void NeuroDataHandler(NeuroDataEventArgs e);
        public event NeuroDataHandler neuroDataEvent;

        //protected virtual void OnNeuroData(NeuroDataEventArgs e)
        //{
        //    //EventHandler<NeuroDataEventArgs> handler = handler_global;
        //    if (handler != null)
        //    {
        //        handler(this, e);
        //    }
        //}
        byte[] buffer;

        private Socket handlerForward;

        static bool connectionOk;
        public BCI bci;

        public void RunNeuroSky(object x)
        {
            connectionOk = false;
            bci = new BCI();
            byte[] bytesL = new Byte[2048];

            ipHostInfoL = Dns.Resolve(Dns.GetHostName());
            ipAddressL = ipHostInfoL.AddressList[0];
            localEndPointL = new IPEndPoint(ipAddressL, 11000);

            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            int bytesRead;
            buffer = new byte[2048];

            try
            {
                //client = new TcpClient("127.0.0.1", 13854);
                client = new TcpClient(AddressFamily.InterNetwork);

                IPAddress[] remoteHost = Dns.GetHostAddresses("127.0.0.1");

                client.BeginConnect(remoteHost, 13854, new
                             AsyncCallback(ConnectCallback), client);

                DateTime localDate = DateTime.Now;
                Console.WriteLine("reading bytes " + localDate.ToString());

                StreamWriter wneuro_eeg = File.AppendText("e:\\neuro_log\\raw_" + localDate.Hour.ToString()
                    + "_" + localDate.Minute.ToString() + ".txt");

                StreamWriter wneuro = File.AppendText("e:\\neuro_log\\sense_" + localDate.Hour.ToString()
                    + "_" + localDate.Minute.ToString() + ".txt");

                //stream = client.GetStream();

                listener.Bind(localEndPointL);
                listener.Listen(100);

                while (true)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallbackL),
                        listener);
                    

                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }


                //    // Sending configuration packet to TGC
                //    if (stream.CanWrite)
                //    {
                //        stream.Write(myWriteBuffer, 0, myWriteBuffer.Length);
                //    }
                //    //AsyncCallback
                //    //stream.BeginRead(,,,)


                //    if (stream.CanRead)
                //    {
                //        Console.WriteLine("reading bytes");

                //        //stream.BeginRead

                //        // This should really be in it's own thread
                //        while (true)
                //        {
                //            bytesRead = stream.Read(buffer, 0, 2048);
                //            string[] packets = Encoding.UTF8.GetString(buffer, 0, bytesRead).Split('\r');
                //            foreach (string s in packets)
                //            {
                //                //Console.WriteLine("   : " + s);
                //                try
                //                {
                //                    JObject jobj = JObject.Parse(s.Trim());
                //                    IDictionary<string, JToken> dictionary = jobj;
                //                    if (dictionary.ContainsKey("eSense"))
                //                    {
                //                        Console.WriteLine("eSense: " + jobj["eSense"].ToString());
                //                        Console.WriteLine("eegPower: " + jobj["eegPower"].ToString());

                //                        wneuro.WriteLine("esense " + jobj["eSense"].ToString());
                //                        wneuro.WriteLine("eegPower " + jobj["eegPower"].ToString());
                //                    }
                //                    if (dictionary.ContainsKey("rawEeg"))
                //                    {
                //                        wneuro_eeg.WriteLine("r " + jobj["rawEeg"].ToString());
                //                    }

                //                }
                //                catch (Exception e) { };

                //                //Console.WriteLine("obj" + jobj.ToString());
                //                //ParseJSON(s.Trim());
                //            }
                //        }
                //    }

                //    System.Threading.Thread.Sleep(5000);

                //    client.Close();
            }
            catch (SocketException se) {
                int k = 10;
            }
        }

        private void ConnectCallback(IAsyncResult result)
        {
            Console.WriteLine("1 st connected!");
            byte[] myWriteBuffer = Encoding.ASCII.GetBytes(@"{""enableRawOutput"": true, ""format"": ""Json""}");

            try
            {
                NetworkStream networkStream = client.GetStream();

                buffer = new byte[client.ReceiveBufferSize];

                
                if (networkStream.CanWrite)
                {
                    networkStream.Write(myWriteBuffer, 0, myWriteBuffer.Length);
                    Console.WriteLine("2 st connected!");
                }

                networkStream.BeginRead(buffer, 0, buffer.Length, ReadCallback, buffer);
            } catch(Exception ex)
            {
             
            }
        }

        private void ReadCallback(IAsyncResult result)
        {     
   
            NetworkStream networkStream;

            try
            {
                networkStream = client.GetStream();           
            }

            catch
            {
             return;

            }         

            byte[] buffer = result.AsyncState as byte[];

            //string data = ASCIIEncoding.ASCII.GetString(buffer, 0, buffer.Length );
            
            string[] packets = ASCIIEncoding.UTF8.GetString(buffer, 0, buffer.Length).Split('\r');
            foreach (string s in packets)
            {
                //Console.WriteLine("D:" + s);

                JObject jobj;
                string att, med;

                dynamic jsonObject = new JObject();

                try
                {
                    jobj = JObject.Parse(s.Trim());
                    IDictionary<string, JToken> dictionary = jobj;

                    if (dictionary.ContainsKey("eSense"))
                    {
                        
                        att = jobj["eSense"]["attention"].ToString();
                        med = jobj["eSense"]["meditation"].ToString();
                        

                        float fmed, fatt;
                        float.TryParse(med, NumberStyles.Any, new CultureInfo("en-US"), out fmed);
                        float.TryParse(att, NumberStyles.Any, new CultureInfo("en-US"), out fatt);

                        
                        bci.AddData( fatt , fmed );
                        jsonObject.speed = bci.getSpeed();
                        jsonObject.frequency = bci.getFrequency();
                        

                        //jsonObject.Add( "speed", bci.getSpeed() );
                        Console.WriteLine("speed " + bci.getSpeed() );
                        Console.WriteLine("frequency " + bci.getFrequency());

                        Console.WriteLine("med-att: " + med + ":" + att);
                    }
               

                    if (handlerForward != null && handlerForward.Connected)
                    {
                        //handlerForward.Connected

                        //justSend(handlerForward, s + "\r");
                        if (jsonObject != null) {
                            string res = (string)jsonObject.ToString(); 

                            string speed = (string) jsonObject.to["speed"];
                            if (speed != null)
                                justSend(handlerForward, res + "\r");
                        }

                    }
                }
                catch (Exception e) { };
            }
            //Do something with the data object here.

            //Then start reading from the network again.

            networkStream.BeginRead(buffer, 0, buffer.Length, ReadCallback, buffer);

        }

        // MAIN CALL BACK FOR NEW
        public void AcceptCallbackL(IAsyncResult ar)
        {
            try {
                // Signal the main thread to continue.
                allDone.Set();

                // Get the socket that handles the client request.
                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);
                if (handlerForward != null)
                {
                    handlerForward.Shutdown(SocketShutdown.Both);
                    handlerForward.Close();
                }
                handlerForward = handler;
                connectionOk = true;

                // Create the state object.
                StateObject state = new StateObject();
                state.workSocket = handler;
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallbackL), state);
            } catch (Exception e) { }
        }


        public static void ReadCallbackL(IAsyncResult ar)
        {
            try
            {
                String content = String.Empty;

                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket handler = state.workSocket;

                // Read data from the client socket. 
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There  might be more data, so store the data received so far.
                    state.sb.Append(Encoding.ASCII.GetString(
                        state.buffer, 0, bytesRead));

                    // Check for end-of-file tag. If it is not there, read 
                    // more data.
                    content = state.sb.ToString();

                    // <EOF> end
                    if (content.IndexOf("EOF") > -1)
                    {
                        // All the data has been read from the 
                        // client. Display it on the console.
                        Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
                            content.Length, content);
                        Send(handler, content);
                    }
                    else {
                        // Not all data received. Get more.
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReadCallbackL), state);
                    }
                }
            }
            catch (Exception e) { };
        }

        private static void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallbackL), handler);
        }

        private static void justSend(Socket handler, String data)
        {
            try
            {
                // Convert the string data to byte data using ASCII encoding.
                byte[] byteData = Encoding.ASCII.GetBytes(data);

                // Begin sending the data to the remote device.
                handler.SendTimeout = 1000;

                
                handler.BeginSend(byteData, 0, byteData.Length, 0,
                    new AsyncCallback(justSendCallbackL), handler);
            }
            catch (Exception e) {
                try
                {
                    if (handler != null)
                    {
                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                    }
                }
                catch (Exception e2) { }
            }
        }

        private static void SendCallbackL(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void justSendCallbackL(IAsyncResult ar)
        {
            if (connectionOk) { 
                Socket handler = (Socket)ar.AsyncState;
                try
                {
                    // Retrieve the socket from the state object.

                    // Complete sending the data to the remote device.
                    handler.SendTimeout = 1000;
                    int bytesSent = handler.EndSend(ar);
                    Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                }
                catch (Exception e)
                {
                    connectionOk = false;
                    Console.WriteLine(e.ToString());
                }
            }
        }


    }
}
