﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;
using System.Net.Sockets;

using Jayrock;
using Jayrock.Json;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using System.Globalization;

using System.Text;
using System.Threading;

// http://developer.neurosky.com/docs/doku.php?id=thinkgear_connector_development_guide
// State object for reading client data asynchronously

namespace NeuroSkyLoad
{
    public class ThreadManager
    {
        public event EventHandler<NeuroDataEventArgs> neuroData;

        static void Main()
        {
            NeuroskyGet device = new NeuroskyGet();
                        
            Thread devThread = new Thread(new ParameterizedThreadStart(device.RunNeuroSky));
            devThread.Start(0);

            while (!devThread.IsAlive) ;

            // Thread.Sleep(8000);
            // devThread.Abort();

            bool neurodev_working = false;

            while (true && (neurodev_working))
            {
                Thread.Sleep(1);
                if (!devThread.IsAlive)
                    neurodev_working = false;

            }
           
            Console.ReadKey();

            //devThread.Join();
        }
    }
}
